#!/bin/bash

set -e

version="${1}"

export KBUILD_BUILD_TIMESTAMP=""
apt-get -qq update
apt-get -qqy install bc bison build-essential ccache cpio debhelper-compat dwarfdump fakeroot flex gawk gcc-13 gcc-14 git kmod libelf-dev libssl-dev lsb-release ncurses-dev python3 rsync xz-utils zstd
export PATH=/lib/ccache:$PATH
if [ ! -d mainline-crack ]; then
    git clone --depth=1 -b cod/mainline/"${version}" git://git.launchpad.net/~ubuntu-kernel-test/ubuntu/+source/linux/+git/mainline-crack
fi
cd mainline-crack
sed -i -e 's,# ARCH:.*,# ARCH: amd64,' -e 's,# FLAVOUR:.*,# FLAVOUR: amd64,' ./debian.master/config/annotations
# use gcc of current running kernel
gcc="$(grep -Eo "gcc-[0-9]+" /boot/config-$(uname -r))"
if [[ -n $gcc ]]; then
    sed -i -re 's/export gcc\?=.*/export gcc?='"${gcc}"'/' debian/rules.d/0-common-vars.mk
fi
fakeroot debian/rules clean
fakeroot debian/rules defaultconfigs
fakeroot debian/rules binary-headers binary-generic binary-perarch

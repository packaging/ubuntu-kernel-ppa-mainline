#!/bin/bash

set -euo pipefail

description="
  Mainline Generic Linux kernel and headers
  This package will always depend on the latest mainline generic Linux kernel and headers."
version="${CI_COMMIT_TAG%+*}"
if [[ "${CI_COMMIT_TAG}" =~ ^.*+.*$ ]]; then
    patchlevel="${CI_COMMIT_TAG#*+}"
else
    patchlevel="1"
fi
arch="$(dpkg --print-architecture)"
IFS='.' read -ra versionsplit <<<"${version}"
if [ "${#versionsplit[@]}" -gt 2 ]; then
    meta_version="${version%.*}"
else
    meta_version="${version}"
fi

apt-get -qqy update
apt-get -qqy install \
    curl \
    libdigest-sha-perl

curl -sLO "https://${KERNEL_URL:-kernel.ubuntu.com/~kernel-ppa/mainline}/v${version}/amd64/CHECKSUMS"
sed -i '/lowlatency/d' CHECKSUMS
mapfile -t packages < <(grep -Eo 'linux-.*.deb' CHECKSUMS | sort -u)
dependencies=()
for package in "${packages[@]}"; do
    curl -sLO "https://${KERNEL_URL:-kernel.ubuntu.com/~kernel-ppa/mainline}/v${version}/amd64/${package}"
    dependencies+=("$(dpkg -I "${package}" | grep 'Package:' | cut -d' ' -f3)")
done
shasum -c CHECKSUMS

if ! command -V nfpm >/dev/null 2>&1; then
    nfpm_version="${NFPM_VERSION:-2.41.1}"
    curl -sfLo /tmp/nfpm.deb "https://github.com/goreleaser/nfpm/releases/download/v${nfpm_version}/nfpm_${nfpm_version}_${arch}.deb"
    apt-get -qqy install /tmp/nfpm.deb
fi

recommends=("morph027-keyring")

# matching gcc for dkms
dpkg --fsys-tarfile linux-modules-"${version}"*-generic_"${version}"*_amd64.deb | tar --wildcards -xO "./boot/config-${version}*-generic" >/tmp/config
gcc="$(grep -oE "gcc-[0-9]+" /tmp/config)"
if [[ -n $gcc ]] && apt-cache show "${gcc}" >/dev/null 2>&1; then
    recommends+=("${gcc}")
fi

# meta package including version
cat >/tmp/meta-version.yaml <<EOF
name: linux-mainline-${meta_version}
version: ${version}+${patchlevel}
arch: ${arch}
section: kernel
version_schema: none
description: |
${description}
maintainer: morph027 <morph@morph027.de>
vendor: Ubuntu Kernel Team https://wiki.ubuntu.com/KernelTeam
homepage: https://${KERNEL_URL:-kernel.ubuntu.com/~kernel-ppa/mainline}/v${version}
recommends:
EOF
for r in "${recommends[@]}"; do
    echo "  - ${r}" >>/tmp/meta-version.yaml
done
echo "depends:" >>/tmp/meta-version.yaml
for d in "${dependencies[@]}"; do
    echo "  - ${d}" >>/tmp/meta-version.yaml
done
nfpm package --config /tmp/meta-version.yaml --packager deb

# meta package
cat >/tmp/meta.yaml <<EOF
name: linux-mainline
version: ${version}+${patchlevel}
arch: ${arch}
section: kernel
version_schema: none
description: |
${description}
maintainer: morph027 <morph@morph027.de>
vendor: Ubuntu Kernel Team https://wiki.ubuntu.com/KernelTeam
homepage: https://${KERNEL_URL:-kernel.ubuntu.com/~kernel-ppa/mainline}/v${version}
depends:
  - linux-mainline-${meta_version}
recommends:
EOF
for r in "${recommends[@]}"; do
    echo "  - ${r}" >>/tmp/meta-version.yaml
done
nfpm package --config /tmp/meta.yaml --packager deb

# signing package
cat >/tmp/signing.yaml <<EOF
name: linux-mainline-signing
version: 24.5.0
arch: all
section: kernel
version_schema: none
description: UEFI Secure Boot Signing for Mainline Generic Linux kernel
maintainer: morph027 <morph@morph027.de>
homepage: https://github.com/berglh/ubuntu-sb-kernel-signing
contents:
  - src: ${CI_PROJECT_DIR}/.packaging/zz-mainline-signing
    dst: /etc/kernel/postinst.d/zz-mainline-signing
    file_info:
      mode: 0764
      owner: root
      group: root
  - src: ${CI_PROJECT_DIR}/.packaging/openssl-mok.cnf
    dst: /etc/ssl/openssl-mok-mainline.cnf
    file_info:
      mode: 0644
      owner: root
      group: root
scripts:
  postinstall: ${CI_PROJECT_DIR}/.packaging/postinstall.sh
  postremove: ${CI_PROJECT_DIR}/.packaging/postremove.sh
recommends:
  - morph027-keyring
depends:
  - coreutils
  - grub-efi-amd64-signed
  - mokutil
  - openssl
  - sbsigntool
  - sed
  - shim-signed
EOF
nfpm package --config /tmp/signing.yaml --packager deb

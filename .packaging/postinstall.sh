#!/bin/sh

set -e

action="${1}"
if [ "${action}" = "configure" ] && [ -z "$2" ]; then
    action="install"
elif [ "${action}" = "configure" ] && [ -n "$2" ]; then
    action="upgrade"
fi

enroll() {
    if [ ! -f /root/.rnd ]; then
        openssl rand -writerand /root/.rnd
    fi
    if [ ! -f /var/lib/shim-signed/mok/MOK-Kernel-Mainline.priv ]; then
        echo "Creating MOK"
        openssl \
            req \
            -config /etc/ssl/openssl-mok-mainline.cnf \
            -new -x509 \
            -newkey rsa:2048 \
            -nodes \
            -days 36500 \
            -outform DER \
            -keyout /var/lib/shim-signed/mok/MOK-Kernel-Mainline.priv \
            -out /var/lib/shim-signed/mok/MOK-Kernel-Mainline.der
        openssl \
            x509 \
            -in /var/lib/shim-signed/mok/MOK-Kernel-Mainline.der \
            -inform DER \
            -outform PEM \
            -out /var/lib/shim-signed/mok/MOK-Kernel-Mainline.pem
    fi
    serial="$(openssl x509 -in /var/lib/shim-signed/mok/MOK-Kernel-Mainline.der -noout -serial | cut -d'=' -f2 | sed 's/../&:/g;s/:$//')"
    if ! mokutil --list-enrolled | grep -qi "${serial}"; then
        echo "Trying to enroll MOK"
        mokutil --generate-hash="password" >/tmp/.mok.hash
        mokutil --hash-file /tmp/.mok.hash --import /var/lib/shim-signed/mok/MOK-Kernel-Mainline.der
        echo "MOK import successful, will be enrolled upon next boot."
    fi
}

install() {
    enroll
}

upgrade() {
    enroll
}

case "${action}" in
"1" | "install")
    install
    ;;
"2" | "upgrade")
    upgrade
    ;;
*)
    :
    ;;
esac

#!/bin/sh

set -e

serial="$(openssl x509 -in /var/lib/shim-signed/mok/MOK-Kernel-Mainline.der -noout -serial | cut -d'=' -f2 | sed 's/../&:/g;s/:$//')"

_mok_delete() {
    if mokutil --list-enrolled | grep -qi "${serial}"; then
        echo "Trying to delete MOK"
        mokutil --generate-hash="password" >/tmp/.mok.hash
        mokutil --hash-file /tmp/.mok.hash --delete /var/lib/shim-signed/mok/MOK-Kernel-Mainline.der
        echo "MOK deletion successful, will be deleted upon next boot."
    fi
}

purge() {
    _mok_delete
    echo "Deleting MOK-Kernel-Mainline from filesystem"
    rm -f \
        /var/lib/shim-signed/mok/MOK-Kernel-Mainline.priv \
        /var/lib/shim-signed/mok/MOK-Kernel-Mainline.der \
        /var/lib/shim-signed/mok/MOK-Kernel-Mainline.pem
}

action="$1"

case "$action" in
"remove")
    :
    ;;
"upgrade")
    :
    ;;
"purge")
    purge
    ;;
esac

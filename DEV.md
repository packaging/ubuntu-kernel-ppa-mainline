## Build

```bash
git clone https://gitlab.com/packaging/ubuntu-kernel-ppa-mainline/
cd ubuntu-kernel-ppa-mainline
docker run --rm -it -e DEBIAN_FRONTEND=noninteractive -v kernel-ccache:/ccache -e CCACHE_DIR=/ccache -w /tmp -v $PWD:/host -v /boot/config-"$(uname -r)":/boot/config-"$(uname -r)":ro ubuntu:noble
/host/.gitlab-ci/build.sh
```

## Test

### Test installation of new kernel version

```bash
cd /tmp
wget --accept-regex '.*amd64.*\.deb' -e robots=off -r -nc https://kernel.ubuntu.com/mainline/v6.10.9/amd64/
sudo apt-get install ./kernel.ubuntu.com/mainline/v6.10.9/amd64/*.deb
```

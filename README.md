## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-ubuntu-kernel-ppa-mainline.asc https://packaging.gitlab.io/ubuntu-kernel-ppa-mainline/gpg.key
```

## Add repo to apt

```bash
apt-get install apt-transport-https
echo "deb [arch=amd64] https://packaging.gitlab.io/ubuntu-kernel-ppa-mainline ubuntu-kernel-mainline main" | sudo tee /etc/apt/sources.list.d/morph027-ubuntu-kernel-ppa-mainline.list
```

## Installation

### UEFI Secure Boot

If you're using UEFI Secure Boot (most likely on modern systems), grub will refuse to load the unsigned kernel.

To sign kernels on your system after installation, just install `linux-mainline-signing` before installing kernels.

```bash
sudo apt-get install linux-mainline-signing
```

If installation succeeds, the blue MokManager screen will appear upon next reboot and allow you to enroll the MOK certficate using the password `password`.

```bash
# latest
sudo apt-get install linux-mainline
# specific version
sudo apt-get install linux-mainline-6.8
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
